define({ "api": [
  {
    "type": "post",
    "url": "/token/",
    "title": "Token validation checker",
    "permission": [
      {
        "name": "user"
      }
    ],
    "version": "0.1.0",
    "name": "GetTokenValidation",
    "group": "Token",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token value to check.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Notify the success of current request.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  success: true,\n  message: \"Token valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokenInvalid",
            "description": "<p>The token is invalid.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "TokenInvalid:",
          "content": "HTTP/1.1 404 Not Found\n{\n  success: false,\n  message: \"Failed to authenticate token.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "router/routes/api/token.js",
    "groupTitle": "Token"
  },
  {
    "type": "post",
    "url": "/users",
    "title": "Add an user",
    "permission": [
      {
        "name": "none"
      }
    ],
    "version": "0.1.0",
    "name": "AddUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>Name of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "address",
            "description": "<p>Address of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "description",
            "description": "<p>Description of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "city",
            "description": "<p>City of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "birthday",
            "description": "<p>Birthday date of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "zip_code",
            "description": "<p>Zip code.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "status",
            "description": "<p>Status relationship of the user: 'S' for single, 'R' for relationship, 'OR' for open relationship, 'NC' for no comments & 'WM' for waiting for miracle.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "work",
            "description": "<p>Work of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "genre",
            "description": "<p>Genre of the user : 'M' for male & 'F' for female.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "looking_for",
            "description": "<p>Type of person what you are looking for : 'W' for woman, 'M' for man & 'B' for both.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "country",
            "description": "<p>Country of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "Image",
            "size": "2 Mo",
            "optional": true,
            "field": "picture",
            "description": "<p>Custom picture for the user : MIME Type has to be : [&quot;image/jpeg&quot;, &quot;image/png&quot;, &quot;image/gif&quot;, &quot;image/tiff&quot;] and accepted extensions [&quot;jpg&quot;, &quot;jpeg&quot;, &quot;png&quot;, &quot;gif&quot;, &quot;tiff&quot;].</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Notify the success of current request.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  success: true,\n  message: 'User created !',\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "WrongArgs",
            "description": "<p>Missing arguments to add the user.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "AlreadyExists",
            "description": "<p>User already exists in database.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>User not found in database.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ServiceUnavailable",
            "description": "<p>Impossible to add an user.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ServerError",
            "description": "<p>Unable to add an user.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "WrongArgs:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  success: false,\n  message: 'Wrong arguments'\n}",
          "type": "json"
        },
        {
          "title": "AlreadyExists:",
          "content": "HTTP/1.1 409 Conflict\n{\n  success: false,\n  message: 'User already exists'\n}",
          "type": "json"
        },
        {
          "title": "UserNotFound:",
          "content": "HTTP/1.1 404 Not Found\n{\n  success: false,\n  message: \"Authentication failed. User not found.\"\n}",
          "type": "json"
        },
        {
          "title": "ServiceUnavailable:",
          "content": "HTTP/1.1 503 Service Unavailable\n{\n  success: false,\n  message: \"error message.\"\n}",
          "type": "json"
        },
        {
          "title": "ServerError:",
          "content": "HTTP/1.1 500 Server Error\n{\n  success: false,\n  message: \"error message.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "router/routes/api/users.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/users/authenticate",
    "title": "Authenticate an user",
    "permission": [
      {
        "name": "none"
      }
    ],
    "version": "0.1.0",
    "name": "AuthenticateUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email to be authentified.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password to be authentified.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Notify the success of current request.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response message.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token of authentification.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Id of user.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  success: true,\n  message: 'Enjoy your token!',\n  token: token,\n  id: user.id\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "WrongPassword",
            "description": "<p>The password isn't correct.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "WrongArgs",
            "description": "<p>Missing arguments to authenticate the user.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>User not found in database.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Unauthorized:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  success: false,\n  message: \"Authentication failed. Wrong password.\"\n}",
          "type": "json"
        },
        {
          "title": "WrongArgs:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  success: false,\n  message: 'Wrong arguments'\n}",
          "type": "json"
        },
        {
          "title": "UserNotFound:",
          "content": "HTTP/1.1 404 Not Found\n{\n  success: false,\n  message: \"Authentication failed. User not found.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "router/routes/api/users.js",
    "groupTitle": "User"
  },
  {
    "type": "delete",
    "url": "/users/:idUser",
    "title": "Delete an user by id",
    "permission": [
      {
        "name": "user"
      }
    ],
    "version": "0.1.0",
    "name": "DeleteUserById",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "idUser",
            "description": "<p>User that you want to delete.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>authentification token is mandatory.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Notify the success of current request.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  success: true,\n  message: 'The user has been deleted.'\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Unauthorized",
            "description": "<p>The token is not valid.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Unauthorized:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  success: false,\n  message: \"Unauthorized.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "router/routes/api/users.js",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/users/:idUser",
    "title": "Edit an user",
    "permission": [
      {
        "name": "user"
      }
    ],
    "version": "0.1.0",
    "name": "EditUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "idUser",
            "description": "<p>User you want to edit.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "password",
            "description": "<p>Password of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>Name of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "address",
            "description": "<p>Address of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "description",
            "description": "<p>Description of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "city",
            "description": "<p>City of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "birthday",
            "description": "<p>Birthday date of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "zip_code",
            "description": "<p>Zip code.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "status",
            "description": "<p>Status relationship of the user: 'S' for single, 'R' for relationship, 'OR' for open relationship, 'NC' for no comments & 'WM' for waiting for miracle.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "work",
            "description": "<p>Work of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "genre",
            "description": "<p>Genre of the user : 'M' for male & 'F' for female.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "looking_for",
            "description": "<p>Type of person what you are looking for : 'W' for woman, 'M' for man & 'B' for both.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "country",
            "description": "<p>Country of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "Image",
            "size": "2 Mo",
            "optional": true,
            "field": "picture",
            "description": "<p>Custom picture for the user : MIME Type has to be : [&quot;image/jpeg&quot;, &quot;image/png&quot;, &quot;image/gif&quot;, &quot;image/tiff&quot;] and accepted extensions [&quot;jpg&quot;, &quot;jpeg&quot;, &quot;png&quot;, &quot;gif&quot;, &quot;tiff&quot;].</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Notify the success of current request.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  success: true,\n  message: 'User updated !'\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Unauthorized",
            "description": "<p>The token is not valid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>User doesn't exist in database.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "BadRequest",
            "description": "<p>Minssing email field to edit user.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ServerError",
            "description": "<p>Impossible to edit the user.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Unauthorized:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  success: false,\n  message: \"Unauthorized.\"\n}",
          "type": "json"
        },
        {
          "title": "NotFound:",
          "content": "HTTP/1.1 404 Not Found\n{\n  success: false,\n  message: \"User not found\"\n}",
          "type": "json"
        },
        {
          "title": "BadRequest:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  success: false,\n  message: \"Email cannot be empty\"\n}",
          "type": "json"
        },
        {
          "title": "ServerError:",
          "content": "HTTP/1.1 500 Server Error\n{\n  success: false,\n  message: \"Error message.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "router/routes/api/users.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/users/",
    "title": "Get all users",
    "permission": [
      {
        "name": "none"
      }
    ],
    "version": "0.1.0",
    "name": "GetUsers",
    "group": "User",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of the user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Address of the user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>City of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "birthday",
            "description": "<p>Birthday date of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "zip_code",
            "description": "<p>Zip code.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Status relationship of the user: 'S' for single, 'R' for relationship, 'OR' for open relationship, 'NC' for no comments & 'WM' for waiting for miracle.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "work",
            "description": "<p>Work of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "genre",
            "description": "<p>Genre of the user : 'M' for male & 'F' for female.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "looking_for",
            "description": "<p>Type of person what you are looking for : 'W' for woman, 'M' for man & 'B' for both.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>Country of the user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "picture",
            "description": "<p>Picture URL of the user.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n    {\n      updatedAt: \"2017-03-23T22:33:14.258Z\",\n      updatedAt: \"2017-03-23T22:33:14.258Z\",\n      createdAt: \"2017-03-23T15:21:58.000Z\",\n      slug: \"BAHA-Hassan\",\n      author: \"577ea485fee4ec632f5c663f\",\n      description: \"yeaaah\",\n      name: \"A big news\",\n      __v: 2,\n      comments: [\n        \"57f816c348165e7e18a84f37\",\n        \"5836192a48e54efc0a9b8695\"\n      ],\n      picture: \"uploads/news/default-news.jpg\"\n     },\n     {\n      ...\n     }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "router/routes/api/users.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/users/:idUser",
    "title": "Get an user by id",
    "permission": [
      {
        "name": "none"
      }
    ],
    "version": "0.1.0",
    "name": "GetUsersById",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "idUser",
            "description": "<p>User you want to get.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of the user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Address of the user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>City of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "birthday",
            "description": "<p>Birthday date of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "zip_code",
            "description": "<p>Zip code.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Status relationship of the user: 'S' for single, 'R' for relationship, 'OR' for open relationship, 'NC' for no comments & 'WM' for waiting for miracle.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "work",
            "description": "<p>Work of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "genre",
            "description": "<p>Genre of the user : 'M' for male & 'F' for female.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "looking_for",
            "description": "<p>Type of person what you are looking for : 'W' for woman, 'M' for man & 'B' for both.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>Country of the user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "picture",
            "description": "<p>Picture URL of the user.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n    {\n      updatedAt: \"2017-03-23T22:33:14.258Z\",\n      updatedAt: \"2017-03-23T22:33:14.258Z\",\n      createdAt: \"2017-03-23T15:21:58.000Z\",\n      slug: \"BAHA-Hassan\",\n      author: \"577ea485fee4ec632f5c663f\",\n      description: \"yeaaah\",\n      name: \"A big news\",\n      __v: 2,\n      comments: [\n        \"57f816c348165e7e18a84f37\",\n        \"5836192a48e54efc0a9b8695\"\n      ],\n      picture: \"uploads/news/default-news.jpg\"\n     },\n     {\n      ...\n     }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "router/routes/api/users.js",
    "groupTitle": "User"
  }
] });
